<?php
    abstract class Database {
        /*
            Clase para manipular bases de datos en general.
            Autor: Pablo Esteban Mendez Domeneche.
        */

        /*
         * Tipo de dato dependiente de la extensión de PHP:
         */
        protected $link; //Link a la conexión abierta.
        
        /*
         * string:
         */
        protected $host; //Host con el que se conecta.

        /*
         * string:
         */
        protected $user; //Usuario.

        /*
         * string:
         */
        protected $password; //Contraseña.

        /*
         * string:
         */
        protected $db; //Base de datos. Por defecto, "".

        /*
         * int:
         */
        protected $port; //Por defecto, 3306.

        /*
         * bool:
         */
        protected $is_connected;
        
        public function __construct($p_host, $p_user, $p_password, $p_db, $p_port) {
            //Al generarse el objeto, intentará la conexión con MySQL:
            $this->is_connected = false;
        }

        abstract public function reconnect();

        public function connected() {
            //Devuelve el valor de la variable privada hay_conexion.
            return $this->is_connected;
        }

        abstract public function disconnect();
        
        abstract public function read($query);

        abstract public function insert($query);
        
        abstract public function delete($query);
        
        abstract public function update($query);
        
        abstract public function other($query);
        
        abstract public function ping();
        
        public function __destruct() {
            //Si por alguna razón se destruyó la instancia, es necesario garantizar la desconexión:
            $this->disconnect();
        }
    }
?>